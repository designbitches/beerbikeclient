import {Component, OnDestroy, OnInit} from '@angular/core';
import {AppService} from './app.service';
import {BeerBikeLogInterface} from './models/beerBikeLog.interface';
import {BeerbikeSpeedInterface} from './models/beerBikeSpeed.interface';
import {BeerBikeSettingsInterface} from './models/beerBikeSetting.interface';
import {BeerBikeResultInterface} from './models/beerBikeResult.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  startAnimation = false;
  interVal: any;
  $status: any;
  $speed: any;
  speed: BeerbikeSpeedInterface;
  status: BeerBikeLogInterface;
  settings: BeerBikeSettingsInterface;
  $settings: any;
  $result: any;
  result: BeerBikeResultInterface;
  currentFooterMsg = 0;


  usrIsBiking = false;

  footerMsgs: string[] = [];
  timeFooterMsgChange = 5500;

  constructor(private appService: AppService) {
  }

  ngOnInit(): void {
    this.startListening();
    this.changeFooterMsg();

    this.$status = this.appService.status.subscribe(status => {
      if (status) {
        this.status = status;
        this.toogleStatus();

      }
    });

    this.$speed = this.appService.speed.subscribe(speed => {
      if (speed) {
        this.speed = speed;
        console.log(this.speed);
      }
    });

    this.$settings = this.appService.settings.subscribe(sett => {
      if (sett) {
        this.settings = sett;
      }
    });
    this.$result = this.appService.result.subscribe(result => {
      if (result) {
        this.result = result;
      }
    });

    this.footerMsgs[0] = 'Ossi aufi aufs Rossi';
    this.footerMsgs[1] = 'Benjamin macht das Rad nicht hin';
    this.footerMsgs[2] = 'Ruppert – rauf aufs Blech Pferd';
    this.footerMsgs[3] = 'Grundi – noch a Rundi?';
    this.footerMsgs[4] = 'Gib Stoff fuer den Hoff';
    this.footerMsgs[5] = 'Wo SAUFEN noch gross geschrieben wird!';
    this.footerMsgs[6] = 'Dem Huber rufen wir heut noch a Uber';
    this.footerMsgs[7] = 'Solange bemuehen - bis dir Reifen gluehen';
    this.footerMsgs[8] = 'So ein Gewimmel - um dem Hoff sein Pimmel';
    this.footerMsgs[9] = 'eine feine Feier - fuer dem Hoff seine Eier';
    this.footerMsgs[10] = 'das sind nicht 20cm ... kleiner Peter';
    this.footerMsgs[11] = 'Lisi trett a bissi!';
    this.footerMsgs[12] = 'ob die Roten heute noch Blau werden?';
    this.footerMsgs[13] = 'Ruppi las tanzen das Puppi';
    this.footerMsgs[14] = 'Alter Schwede, wo ist die Bombe';
    this.footerMsgs[15] = 'Petzen hohl den Hammer raus';
    this.footerMsgs[16] = 'Pedal statt Klimmzug';
    this.footerMsgs[17] = 'der Hans der kanns';
    this.footerMsgs[18] = 'Ita noch an Lita';
    this.footerMsgs[19] = 'noch a Stickl, Krikl';
    this.footerMsgs[20] = 'keine Sau will ins Team blau';
    this.footerMsgs[21] = 'team Rot, säuft Wasser zur Not';
    this.footerMsgs[22] = 'geht scho Reisackerl';
    this.footerMsgs[23] = 'Dem Ronny verblaest es den Ponny';
    this.footerMsgs[23] = 'Moji aus Bushehr hätte lieber Geschlechtsverkehr';
    this.footerMsgs[24] = 'Rolli zum wohli';
    this.footerMsgs[25] = 'Kathl rauf – auf den Sattl';
    this.footerMsgs[26] = 'die Julie hat Kondi wie a Muli';
    this.footerMsgs[27] = 'Allerhand – geehrte Frau vom Arbeitsamt';



    this.footerMsgs = this.shuffle(this.footerMsgs);

  }
  ngOnDestroy(): void {
  }

  toogleStatus() {
    if (this.status.command === 'start' && this.usrIsBiking === false) {
      this.usrIsBiking = true;
      this.startPimperBoogie();

    }
    if (this.status.command === 'stop' && this.usrIsBiking === true) {
      this.usrIsBiking = false;
      this.startPimperBoogie();
    }

  }

  startPimperBoogie() {
    this.startAnimation = true;
    setTimeout(() => {
      this.startAnimation = false;
    }, 1000);

  }

  shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
  }

  changeFooterMsg() {
    setInterval(() => {

      // this.footerMsgs = this.shuffle(this.footerMsgs);
      if (this.footerMsgs.length === this.currentFooterMsg) {
        this.currentFooterMsg = 0;
      } else {
        this.currentFooterMsg++;
      }


    }, this.timeFooterMsgChange);
  }

  startListening() {
    this.interVal = setInterval(() => {
      this.appService.loadData();
      this.appService.loadSettings();
      this.appService.loadResult();
      if (this.usrIsBiking) {
        this.appService.loadSpeedData();
      }

    }, 1000);
  }

  calcDegForPimper() {
    if (!this.usrIsBiking) {
      return 0;
    }
    if (!this.speed) {
      return 0;
    }
    const maxSpeed = 100;
    const maxDegRed = 170;
    const maxDegBlue = 190;
    const minBlue = 355;

    if (this.status.team === 'red') {
      return maxDegRed / maxSpeed * this.speed.speed;
    }
    if (this.status.team === 'blue') {
      const diff = minBlue - maxDegBlue;
      return minBlue - (diff / 100 * this.speed.speed);
    }


    return 328;
  }

  calcFillHeight(color: string) {
    if (!this.settings) {
      return  0;
    }
    if (!this.result) {
      return  0;
    }
    if (color === 'red') {
      return 100 / this.settings.maxKm * this.result.roundsTeamRed;
    }
    if (color === 'blue') {
      return 100 / this.settings.maxKm * this.result.roundsTeamBlue;
    }
    return 0;


  }


}




import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {environment} from '../environments/environment';
import {BehaviorSubject} from 'rxjs';
import {BeerBikeLogInterface} from './models/beerBikeLog.interface';
import {BeerbikeSpeedInterface} from './models/beerBikeSpeed.interface';
import {BeerBikeSettingsInterface} from './models/beerBikeSetting.interface';
import {BeerBikeResultInterface} from './models/beerBikeResult.interface';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  status: BehaviorSubject<BeerBikeLogInterface> = new BehaviorSubject<BeerBikeLogInterface>(null);
  speed: BehaviorSubject<BeerbikeSpeedInterface> = new BehaviorSubject<BeerbikeSpeedInterface>(null);
  settings: BehaviorSubject<BeerBikeSettingsInterface> = new BehaviorSubject<BeerBikeSettingsInterface>(null);
  result: BehaviorSubject<BeerBikeResultInterface> = new BehaviorSubject<BeerBikeResultInterface>(null);

  constructor(private http: HttpClient) { }

  loadData() {
    this.http.get(environment.API + 'status', {headers: this.get_Header()})
      .subscribe(
        (data: BeerBikeLogInterface) => {
          if (data) {
            this.status.next(data);
          }
        },
        error => console.log(error)
      );
  }

  loadSettings() {
    this.http.get(environment.API + 'settings', {headers: this.get_Header()})
      .subscribe(
        (data: BeerBikeSettingsInterface) => {
          if (data) {
            this.settings.next(data);
          }
        },
        error => console.log(error)
      );
  }

  loadResult() {
    this.http.get(environment.API + 'result', {headers: this.get_Header()})
      .subscribe(
        (data: BeerBikeResultInterface) => {
          if (data) {
            this.result.next(data);
          }
        },
        error => console.log(error)
      );
  }
  loadSpeedData() {
    this.http.get(environment.API + 'speed', {headers: this.get_Header()})
      .subscribe(
        (data: BeerbikeSpeedInterface) => {
          if (data) {
            this.speed.next(data);
          }
        },
        error => console.log(error)
      );
  }

  get_Header() {
    return new HttpHeaders({'Content-Type': 'application/json'});
  }
}

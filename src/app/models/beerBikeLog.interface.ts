export interface BeerBikeLogInterface {
  _id: string;
  team: string; // red, blue
  command: string;  // start, stop
  rounds: number; // number of rounds
  time: Date;
  timeStamp: number;
}

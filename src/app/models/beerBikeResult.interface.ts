export interface BeerBikeResultInterface {
  roundsTeamRed: number;
  roundsTeamBlue: number;
}

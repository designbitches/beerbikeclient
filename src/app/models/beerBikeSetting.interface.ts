export interface BeerBikeSettingsInterface {
  _id: string;
  mode: string;
  maxKm: number;
}

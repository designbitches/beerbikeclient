export interface BeerbikeSpeedInterface {
  _id: string;
  speed: number;
  time: Date;
  timeStamp: number;

}
